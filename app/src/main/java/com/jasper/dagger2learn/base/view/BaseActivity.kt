package com.jasper.dagger2learn.base.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.inputmethod.InputMethodManager
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.jasper.dagger2learn.common.ActivityStackManager
import com.jasper.dagger2learn.databinding.ActivityMainBinding
import com.jasper.dagger2learn.utils.ToastUtil
import com.jasper.dagger2learn.utils.statusbarutil.StatusBarUtil
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.impl.LoadingPopupView
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity


abstract class BaseActivity : DaggerAppCompatActivity(), MvpView {

    private var loadingDialog: LoadingPopupView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        StatusBarUtil.setImmersiveStatusBar(this, true)
        // 添加Activity到堆栈
        ActivityStackManager.getInstance().onCreated(this)
        super.onCreate(savedInstanceState)
        initPresenter()
        initLoadingView()
    }

    //预留给BaseMVPActivity
    open fun initPresenter() {

    }


    private fun initLoadingView() {
        loadingDialog = XPopup.Builder(this).asLoading("请稍候...")
    }

    override fun showToastLong(msg: String) {
        ToastUtil.showLong(msg)
    }

    override fun showToastLong(@StringRes id: Int) {
        ToastUtil.showLong(id)
    }

    override fun showToastShort(msg: String) {
        ToastUtil.showShort(msg)
    }

    override fun showToastShort(@StringRes id: Int) {
        ToastUtil.showShort(id)
    }


    override fun onDestroy() {
        ActivityStackManager.getInstance().onDestroyed(this)
        // 销毁dialog
        if (null != loadingDialog && loadingDialog!!.isShow) {
            loadingDialog!!.dismiss()
        }
        loadingDialog = null

        super.onDestroy()
    }


    /**
     * 隐藏软键盘
     */
    open fun hideSoftKeyboard() {
        // 隐藏软键盘，避免软键盘引发的内存泄露
        val view = currentFocus
        if (view != null) {
            val manager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            manager?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    private fun showLoadingView() {
        if (null != loadingDialog) {
            loadingDialog!!.show()
        } else {
            initLoadingView()
            loadingDialog!!.show()
        }
    }

    private fun showLoadingView(msg: String) {
        if (null != loadingDialog) {
            if (msg.isNotEmpty()) {
                loadingDialog!!.setTitle(msg)
            }
            loadingDialog!!.show()
        } else {
            initLoadingView()
            if (msg.isNotEmpty()) {
                loadingDialog!!.setTitle(msg)
            }
            loadingDialog!!.show()
        }
    }

    private fun hideLoadingView() {
        if (null != loadingDialog && loadingDialog!!.isShow) {
            loadingDialog!!.dismiss()
            loadingDialog!!.setTitle("请稍候...")
        }
    }

    override fun showLoading() {
        showLoadingView()
    }

    override fun showLoading(msg: String) {
        showLoadingView(msg)
    }

    override fun hideLoading() {
        hideLoadingView()
    }

}