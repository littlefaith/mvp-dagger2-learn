package com.jasper.dagger2learn.base

data class BaseBean(
    val msg: String,
    val code: Int,
    val data: Any
)