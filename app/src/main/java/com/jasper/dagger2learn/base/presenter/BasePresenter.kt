package com.jasper.dagger2learn.base.presenter

import com.jasper.dagger2learn.base.baseModel.MvpModel
import com.jasper.dagger2learn.base.view.MvpView

open class BasePresenter<V : MvpView, M : MvpModel> : MvpPresenter<V, M> {
    private var view: V? = null

    override fun attachView(view: V) {
        this.view = view
    }

    override fun detachView() {
        view = null
    }

    override fun isAttached(): Boolean {
        return view != null
    }
}