package com.jasper.dagger2learn.base.baseModel

open class BaseModel:MvpModel {
    override fun isUserLoggedIn(): Boolean {
        return false
    }

    override fun performUserLogout() {
    }
}