package com.jasper.dagger2learn.base.presenter

import com.jasper.dagger2learn.base.baseModel.MvpModel
import com.jasper.dagger2learn.base.view.MvpView

interface MvpPresenter<V : MvpView, M : MvpModel> {
    fun attachView(view: V)

    fun detachView()

    fun isAttached(): Boolean

}