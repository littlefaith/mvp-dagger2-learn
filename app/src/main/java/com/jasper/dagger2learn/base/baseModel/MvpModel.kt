package com.jasper.dagger2learn.base.baseModel

interface MvpModel {
    fun isUserLoggedIn(): Boolean

    fun performUserLogout()
}