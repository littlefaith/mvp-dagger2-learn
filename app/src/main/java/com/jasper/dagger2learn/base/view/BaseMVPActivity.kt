package com.jasper.dagger2learn.base.view

import android.os.Bundle
import com.jasper.dagger2learn.base.baseModel.MvpModel
import com.jasper.dagger2learn.base.presenter.BasePresenter

abstract class BaseMVPActivity<V : MvpView, M : MvpModel, P : BasePresenter<V, M>> : BaseActivity(), MvpView {

    private var basePresenter: P? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun initPresenter() {
        basePresenter = getPresenter()
        basePresenter?.attachView(this as V)
    }

    abstract fun getPresenter(): P


    override fun onDestroy() {
        // 在activity销毁时，解绑activity和presenter
        if (basePresenter != null) {
            basePresenter?.detachView()
            basePresenter = null
        }
        super.onDestroy()
    }

}
