package com.jasper.dagger2learn.base.view

interface MvpView {
    fun showLoading()
    fun showLoading(msg:String)
    fun hideLoading()
    fun showToastLong(msg: String)
    fun showToastLong(id: Int)
    fun showToastShort(msg: String)
    fun showToastShort(id: Int)
}