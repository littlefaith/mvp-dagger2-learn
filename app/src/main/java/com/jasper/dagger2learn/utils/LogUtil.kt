package com.jasper.dagger2learn.utils

import android.util.Log
import com.jasper.dagger2learn.BuildConfig

object LogUtil {
    fun v(tag: String, msg: String): Int {
        if (BuildConfig.DEBUG) {
            return Log.v(tag, msg);
        }
        return 0;
    }

    fun v(tag: String, msg: String, tr: Throwable): Int {
        if (BuildConfig.DEBUG) {
            return Log.v(tag, msg, tr);
        }
        return 0;
    }

    fun d(tag: String, msg: String): Int {
        if (BuildConfig.DEBUG) {
            return Log.d(tag, msg);
        }
        return 0;
    }

    fun d(tag: String, msg: String, tr: Throwable): Int {
        if (BuildConfig.DEBUG) {
            return Log.d(tag, msg, tr);
        }
        return 0;
    }

    fun i(tag: String, msg: String): Int {
        if (BuildConfig.DEBUG) {
            return Log.i(tag, msg);
        }
        return 0;
    }

    fun i(tag: String, msg: String, tr: Throwable): Int {
        if (BuildConfig.DEBUG) {
            return Log.i(tag, msg, tr);
        }
        return 0;
    }

    fun w(tag: String, msg: String): Int {
        if (BuildConfig.DEBUG) {
            return Log.w(tag, msg);
        }
        return 0;
    }

    fun w(tag: String, msg: String, tr: Throwable): Int {
        if (BuildConfig.DEBUG) {
            return Log.w(tag, msg, tr);
        }
        return 0;
    }

    fun e(tag: String, msg: String): Int {
        if (BuildConfig.DEBUG) {
            return Log.e(tag, msg);
        }
        return 0;
    }

    fun e(tag: String, msg: String, tr: Throwable): Int {
        if (BuildConfig.DEBUG) {
            return Log.e(tag, msg, tr);
        }
        return 0;
    }
}