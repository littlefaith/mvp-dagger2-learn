package com.jasper.dagger2learn.utils

import android.app.ActivityManager
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Process
import android.view.View
import android.view.Window
import android.view.WindowManager

object Utils {

    //设置界面全屏
    fun setFullScreenWindowLayout(window: Window) {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE //or 和java的 | 符号一样
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = Color.TRANSPARENT
        }
        //设置页面全屏显示
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            val layoutParams = window.attributes
            layoutParams.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
            window.attributes = layoutParams
        }
    }


    /**
     * 判断该进程ID是否属于该进程名
     * @param context
     * @param pid 进程ID
     * @param p_name 进程名
     * @return true属于该进程名
     */
    fun isPidOfProcessName(context: Context, pid: Int, p_name: String?): Boolean {
        if (p_name == null) return false
        var isMain = false
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        //遍历所有进程
        for (process in am.runningAppProcesses) {
            if (process.pid == pid) {
                //进程ID相同时判断该进程名是否一致
                if (process.processName == p_name) {
                    isMain = true
                }
                break
            }
        }
        return isMain
    }

    /**
     * 获取主进程名
     * @param context 上下文
     * @return 主进程名
     */
    @Throws(PackageManager.NameNotFoundException::class)
    fun getMainProcessName(context: Context): String? {
        return context.packageManager.getApplicationInfo(context.packageName, 0).processName
    }

    /**
     * 判断是否主进程
     * @param context 上下文
     * @return true是主进程
     */
    @Throws(PackageManager.NameNotFoundException::class)
    fun isMainProcess(context: Context): Boolean {
        return isPidOfProcessName(context, Process.myPid(), getMainProcessName(context))
    }

}

