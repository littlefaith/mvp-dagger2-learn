package com.jasper.dagger2learn.di.module

import com.jasper.dagger2learn.di.ActivityScope
import com.jasper.dagger2learn.ui.view.LoginActivity
import com.jasper.dagger2learn.ui.view.MainActivity
import com.jasper.dagger2learn.ui.view.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindModule {

    @ActivityScope
    //subComponent对应的model是SplashModule，这里省略掉了SplashComponent
    @ContributesAndroidInjector(modules = [SplashModule::class])
    abstract fun splashActivityInjector(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginModule::class])
    abstract fun loginActivityInjector(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun mainActivityInjector(): MainActivity

}