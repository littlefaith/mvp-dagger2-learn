package com.jasper.dagger2learn.di.module

import com.jasper.dagger2learn.di.ActivityScope
import com.jasper.dagger2learn.ui.contracts.SplashContract
import com.jasper.dagger2learn.ui.model.SplashModel
import com.jasper.dagger2learn.ui.presenter.SplashPresenter
import com.jasper.dagger2learn.ui.view.SplashActivity
import dagger.Module
import dagger.Provides

@Module
class SplashModule {
    @ActivityScope
    @Provides
    internal fun provideSplashView(splashActivity: SplashActivity): SplashContract.ISplashView =
        splashActivity

    @ActivityScope
    @Provides
    internal fun provideSplashModel(splashModel: SplashModel): SplashContract.ISplashModel =
        splashModel

    @ActivityScope
    @Provides
    internal fun provideSplashPresenter(splashPresenter: SplashPresenter<SplashContract.ISplashView, SplashModel>)
            : SplashContract.ISplashPresenter<SplashContract.ISplashView, SplashModel> {
        return splashPresenter
    }

}