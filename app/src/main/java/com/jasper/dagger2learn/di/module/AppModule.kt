package com.jasper.dagger2learn.di.module

import android.app.Application
import android.content.Context
import com.jasper.dagger2learn.APP
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class AppModule() {

    @Binds
    abstract fun provideApplication(application: Application): Context
}