package com.jasper.dagger2learn.di.module

import com.jasper.dagger2learn.di.ActivityScope
import com.jasper.dagger2learn.ui.contracts.MainContract
import com.jasper.dagger2learn.ui.model.MainModel
import com.jasper.dagger2learn.ui.presenter.MainPresenter
import com.jasper.dagger2learn.ui.view.MainActivity
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
class MainModule {

    @ActivityScope
    @Provides
    internal fun provideMainView(mainActivity: MainActivity): MainContract.MainView = mainActivity

    @ActivityScope
    @Provides
    internal fun provideMainModel(mainModel: MainModel): MainContract.IMainModel = mainModel

    @ActivityScope
    @Provides
    internal fun provideMainPresenter(mainPresenter: MainPresenter<MainContract.MainView, MainModel>)
            : MainContract.IMainPresenter<MainContract.MainView, MainModel> = mainPresenter
}