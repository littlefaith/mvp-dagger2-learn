package com.jasper.dagger2learn.di.component

import android.app.Application
import com.jasper.dagger2learn.APP
import com.jasper.dagger2learn.di.module.ActivityBindModule
import com.jasper.dagger2learn.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

//APP Component需要声明为单例模式
@Singleton
//AndroidInjectionModule是dagger.android扩展包里的module，需要引入
@Component(modules = [AndroidInjectionModule::class, AppModule::class, ActivityBindModule::class])
interface AppComponent : AndroidInjector<APP> {
    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<APP>

//    override fun inject(app: APP)
}