package com.jasper.dagger2learn.di.module

import com.jasper.dagger2learn.di.ActivityScope
import com.jasper.dagger2learn.ui.contracts.LoginContract
import com.jasper.dagger2learn.ui.model.LoginModel
import com.jasper.dagger2learn.ui.presenter.LoginPresenter
import com.jasper.dagger2learn.ui.view.LoginActivity
import dagger.Module
import dagger.Provides

@Module
class LoginModule {
    @ActivityScope
    @Provides
    internal fun provideLoginView(loginActivity: LoginActivity): LoginContract.ILoginView =
        loginActivity

    @ActivityScope
    @Provides
    internal fun provideLoginModel(loginModel: LoginModel): LoginContract.ILoginModel =
        loginModel

    @ActivityScope
    @Provides
    internal fun provideLoginPresenter(loginPresenter: LoginPresenter<LoginContract.ILoginView, LoginModel>)
            : LoginContract.ILoginPresenter<LoginContract.ILoginView, LoginModel> {
        return loginPresenter
    }

}