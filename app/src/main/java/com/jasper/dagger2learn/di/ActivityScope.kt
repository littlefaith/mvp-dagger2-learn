package com.jasper.dagger2learn.di

import javax.inject.Scope

@Scope
@MustBeDocumented
@Retention
annotation class ActivityScope