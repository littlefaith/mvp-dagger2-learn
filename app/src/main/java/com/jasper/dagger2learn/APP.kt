package com.jasper.dagger2learn

import android.content.Context
import androidx.multidex.MultiDex
import com.jasper.dagger2learn.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class APP : DaggerApplication() {

    companion object {
        lateinit var context: Context
        lateinit var app: APP
    }

    override fun onCreate() {
        super.onCreate()
        context = this.applicationContext
        app = this
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(this)
    }
}