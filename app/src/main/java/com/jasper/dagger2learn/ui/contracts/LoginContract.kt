package com.jasper.dagger2learn.ui.contracts

import android.sax.EndElementListener
import com.jasper.dagger2learn.base.baseModel.MvpModel
import com.jasper.dagger2learn.base.presenter.MvpPresenter
import com.jasper.dagger2learn.base.view.MvpView

interface LoginContract {
    interface ILoginModel : MvpModel {
        fun doLogin(account: String, pwd: String, listener: Listeners)

        interface Listeners {
            fun onLoginSuccess()
        }
    }

    interface ILoginPresenter<V : ILoginView, M : ILoginModel> : MvpPresenter<V, M> {
        fun doLogin(account: String, pwd: String)
    }

    interface ILoginView : MvpView {
        fun navToMain()
    }
}