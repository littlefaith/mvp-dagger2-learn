package com.jasper.dagger2learn.ui.model

import android.content.Context
import android.os.Handler
import com.jasper.dagger2learn.APP
import com.jasper.dagger2learn.base.baseModel.BaseModel
import com.jasper.dagger2learn.ui.contracts.SplashContract
import javax.inject.Inject

//这里调用网络获取数据
class SplashModel @Inject constructor() : BaseModel(), SplashContract.ISplashModel {
}