package com.jasper.dagger2learn.ui.model

import com.jasper.dagger2learn.base.baseModel.BaseModel
import com.jasper.dagger2learn.ui.contracts.MainContract
import javax.inject.Inject

class MainModel @Inject constructor() : BaseModel(), MainContract.IMainModel {
}