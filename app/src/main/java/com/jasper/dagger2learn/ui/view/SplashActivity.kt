package com.jasper.dagger2learn.ui.view

import android.Manifest
import android.content.Intent
import android.os.Bundle
import com.jasper.dagger2learn.R
import com.jasper.dagger2learn.base.presenter.BasePresenter
import com.jasper.dagger2learn.base.view.BaseMVPActivity
import com.jasper.dagger2learn.databinding.ActivitySplashBinding
import com.jasper.dagger2learn.ui.contracts.SplashContract
import com.jasper.dagger2learn.ui.model.SplashModel
import com.jasper.dagger2learn.ui.presenter.SplashPresenter
import com.jasper.dagger2learn.utils.LogUtil
import com.jasper.dagger2learn.utils.Utils
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject

class SplashActivity :
    BaseMVPActivity<SplashContract.ISplashView, SplashModel, BasePresenter<SplashContract.ISplashView, SplashModel>>(),
    SplashContract.ISplashView, EasyPermissions.PermissionCallbacks {
    companion object {
        const val REQUEST_PHONE_PERMISSIONS = 0x01
        const val TAG = "SplashActivity"
    }

    @Inject
    lateinit var presenter: SplashPresenter<SplashContract.ISplashView, SplashModel>
    lateinit var binding: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.setFullScreenWindowLayout(window)
        binding = ActivitySplashBinding.bind(layoutInflater.inflate(R.layout.activity_splash, null))
        setContentView(binding.root)
        initData()
        initView()
        initEvent()
    }

    override fun getPresenter(): BasePresenter<SplashContract.ISplashView, SplashModel> {
        return presenter
    }

//    override fun getLayoutId(): Int {
//        return R.layout.activity_splash
//    }

    fun initData() {
        if (EasyPermissions.hasPermissions(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        ) {
            presenter.nvaToNextPage()
        } else {
            EasyPermissions.requestPermissions(
                this,
                "因功能需要，请同意如下权限",
                REQUEST_PHONE_PERMISSIONS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        }
    }

    fun initView() {
    }

    fun initEvent() {
    }

    override fun navToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun navToLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (requestCode == REQUEST_PHONE_PERMISSIONS) {
            if (perms.size >= 2) {
                presenter.nvaToNextPageDirectly()
            } else {
                showToastLong("请允许所有权限并重启应用")
                finish()
            }
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        if (requestCode == REQUEST_PHONE_PERMISSIONS) {
            showToastLong("请允许权限并重启应用")
            finish()
        }
    }

}