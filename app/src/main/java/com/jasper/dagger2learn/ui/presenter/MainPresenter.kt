package com.jasper.dagger2learn.ui.presenter

import com.jasper.dagger2learn.base.baseModel.BaseModel
import com.jasper.dagger2learn.base.presenter.BasePresenter
import com.jasper.dagger2learn.ui.contracts.MainContract
import com.jasper.dagger2learn.ui.model.MainModel
import javax.inject.Inject

class MainPresenter<V : MainContract.MainView, M : MainContract.IMainModel> @Inject constructor(val view: V, model: M) :
    BasePresenter<V, M>(), MainContract.IMainPresenter<V, M> {
}