package com.jasper.dagger2learn.ui.presenter

import android.os.Handler
import com.jasper.dagger2learn.APP
import com.jasper.dagger2learn.base.presenter.BasePresenter
import com.jasper.dagger2learn.ui.contracts.SplashContract
import javax.inject.Inject

class SplashPresenter<V : SplashContract.ISplashView, T : SplashContract.ISplashModel> @Inject constructor(
    val view: V,
    private val model: T

) :
    BasePresenter<V, T>(),
    SplashContract.ISplashPresenter<V, T> {
    var isLogin = false
    override fun isUserLogin(): Boolean {
        return isLogin
    }

    override fun nvaToNextPage() {
        //启动页两秒延迟
        Handler(APP.context.mainLooper).postDelayed({
//            isLogin = true
            nvaToNextPageDirectly()
        }, 2000)
    }

    override fun nvaToNextPageDirectly() {
        if (isUserLogin()) {
            view.navToMain()
        } else {
            view.navToLogin()
        }
    }


}