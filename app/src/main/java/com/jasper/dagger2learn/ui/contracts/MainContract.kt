package com.jasper.dagger2learn.ui.contracts

import com.jasper.dagger2learn.base.baseModel.MvpModel
import com.jasper.dagger2learn.base.presenter.MvpPresenter
import com.jasper.dagger2learn.base.view.MvpView

interface MainContract {
    interface MainView : MvpView {}
    interface IMainModel : MvpModel {}
    interface IMainPresenter<V : MainView, M : IMainModel> : MvpPresenter<V, M> {}
}