package com.jasper.dagger2learn.ui.view

import android.content.Intent
import android.os.Bundle
import com.jasper.dagger2learn.R
import com.jasper.dagger2learn.base.presenter.BasePresenter
import com.jasper.dagger2learn.base.view.BaseMVPActivity
import com.jasper.dagger2learn.databinding.ActivityLoginBinding
import com.jasper.dagger2learn.databinding.ActivityMainBinding
import com.jasper.dagger2learn.databinding.ActivitySplashBinding
import com.jasper.dagger2learn.ui.contracts.LoginContract
import com.jasper.dagger2learn.ui.model.LoginModel
import com.jasper.dagger2learn.ui.presenter.LoginPresenter
import com.zysoft.mwreader.utils.extentions.singleClick
import javax.inject.Inject

class LoginActivity :
    BaseMVPActivity<LoginContract.ILoginView, LoginModel, BasePresenter<LoginContract.ILoginView, LoginModel>>(),
    LoginContract.ILoginView {
    lateinit var binding: ActivityLoginBinding

    @Inject
    lateinit var presenter: LoginPresenter<LoginContract.ILoginView, LoginModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.bind(layoutInflater.inflate(R.layout.activity_login, null))
        setContentView(binding.root)
        initData()
        initView()
        initEvent()
    }

    override fun getPresenter(): BasePresenter<LoginContract.ILoginView, LoginModel> {
        return presenter
    }

//    override fun getLayoutId(): Int {
//        return R.layout.activity_login
//    }

    private fun initData() {
    }

    private fun initView() {
    }

    private fun initEvent() {
        binding.btnLogin.singleClick {
            presenter.doLogin("", "")
        }
    }

    override fun navToMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}