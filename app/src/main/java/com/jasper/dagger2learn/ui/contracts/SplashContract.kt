package com.jasper.dagger2learn.ui.contracts

import android.sax.EndElementListener
import com.jasper.dagger2learn.base.baseModel.MvpModel
import com.jasper.dagger2learn.base.presenter.MvpPresenter
import com.jasper.dagger2learn.base.view.MvpView

interface SplashContract {
    interface ISplashModel : MvpModel {
//        fun doLogin(account: String, pwd: String, listener: Listeners)
//
//        interface Listeners {
//            fun onLoginSuccess()
//        }
    }

    interface ISplashPresenter<V : ISplashView, T : ISplashModel> : MvpPresenter<V, T> {
        fun isUserLogin(): Boolean

        fun nvaToNextPage()
        fun nvaToNextPageDirectly()
    }

    interface ISplashView : MvpView {
        fun navToMain()
        fun navToLogin()
    }
}