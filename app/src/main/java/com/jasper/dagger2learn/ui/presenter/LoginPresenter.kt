package com.jasper.dagger2learn.ui.presenter

import android.os.Handler
import com.jasper.dagger2learn.APP
import com.jasper.dagger2learn.base.presenter.BasePresenter
import com.jasper.dagger2learn.ui.contracts.LoginContract
import javax.inject.Inject

class LoginPresenter<V : LoginContract.ILoginView, M : LoginContract.ILoginModel> @Inject constructor(val view: V, val model: M) :
    BasePresenter<V, M>(), LoginContract.ILoginPresenter<V, M>, LoginContract.ILoginModel.Listeners {
    override fun doLogin(account: String, pwd: String) {
        view.showLoading()
        //模拟登陆
        model.doLogin(account, pwd, this)
    }

    override fun onLoginSuccess() {
        view.hideLoading()
        view.navToMain()
    }
}