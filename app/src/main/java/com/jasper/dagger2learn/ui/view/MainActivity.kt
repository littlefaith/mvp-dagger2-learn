package com.jasper.dagger2learn.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jasper.dagger2learn.R
import com.jasper.dagger2learn.base.presenter.BasePresenter
import com.jasper.dagger2learn.base.view.BaseMVPActivity
import com.jasper.dagger2learn.databinding.ActivityMainBinding
import com.jasper.dagger2learn.ui.contracts.MainContract
import com.jasper.dagger2learn.ui.model.MainModel
import com.jasper.dagger2learn.ui.presenter.MainPresenter
import javax.inject.Inject

class MainActivity : BaseMVPActivity<MainContract.MainView, MainModel, BasePresenter<MainContract.MainView, MainModel>>(),
    MainContract.MainView {
    @Inject
    lateinit var presenter: MainPresenter<MainContract.MainView, MainModel>
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.bind(layoutInflater.inflate(R.layout.activity_main, null))
        setContentView(binding.root)
    }

    override fun getPresenter(): BasePresenter<MainContract.MainView, MainModel> {
        return presenter
    }
}